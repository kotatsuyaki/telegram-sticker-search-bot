# Telegram Sticker Search Bot

![](https://github.com/kouakitaki/telegram-sticker-search-bot/actions/workflows/build_nix.yml/badge.svg)

> Warning: This project has not been updated in a while, and the dependencies are all outdated.

A simple bot that allow users to associate arbitrary stickers with tags and use inline queries to
search for stickers.
